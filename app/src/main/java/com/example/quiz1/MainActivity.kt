package com.example.quiz1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var user: User


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        user = User(
            firstNameEditText.toString(),
            lastNameEditText.toString(),
            emailEditText.toString()
        )
        addButton.setOnClickListener {
            if (emailEditText.text.isEmpty() || firstNameEditText.text.isEmpty() || lastNameEditText.text.isEmpty() || ageEditText.text.isEmpty()) {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT)
                    .show()
            } else {

                addUser()
            }
        }
        removeButton.setOnClickListener {
            if (emailEditText.text.isEmpty() || firstNameEditText.text.isEmpty() || lastNameEditText.text.isEmpty() || ageEditText.text.isEmpty()) {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                removeUser()
            }
        }
    }


    private fun addUser() {
        if (user in User.userList) {
            Toast.makeText(applicationContext, "User Already Exist!", Toast.LENGTH_SHORT).show()
        } else{
            User.userList.add(user)
        Toast.makeText(applicationContext, "Added Successfully!", Toast.LENGTH_SHORT).show()

    }}

    private fun removeUser() {
        if (user in User.userList) {
            User.userList.remove(user)
            d("list", User.userList.toString())
            Toast.makeText(applicationContext, "Removed Successfully!", Toast.LENGTH_SHORT).show()

        } else
            Toast.makeText(applicationContext, "User Don't Exist!", Toast.LENGTH_SHORT).show()
        d("list", User.userList.toString())

    }
}
